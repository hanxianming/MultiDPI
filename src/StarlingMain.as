package
{
	import scalar.display.DPIImage;
	import scalar.display.DPISprite;
	import scalar.display.DPITextField;
	import scalar.resource.DPIResource;
	
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public final class StarlingMain extends Sprite
	{
		public function StarlingMain()
		{
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init():void
		{
			var background:DPIImage = new DPIImage(DPIResource.instance.getTexture("background"));
			addChild(background);
			background.dpiX = 0;
			background.dpiY = 0;
			
			var starling_sheet:DPIImage = new DPIImage(DPIResource.instance.getTexture("starling_sheet"));
			addChild(starling_sheet);
			starling_sheet.dpiX = 51;
			starling_sheet.dpiY = 51;
			
			var starling_round:DPIImage = new DPIImage(DPIResource.instance.getTexture("starling_round"));
			addChild(starling_round);
			starling_round.dpiX = 60;
			starling_round.dpiY = starling_sheet.dpiY + starling_sheet.dpiH;
			
			var benchmark_object:DPIImage = new DPIImage(DPIResource.instance.getTexture("benchmark_object"));
			addChild(benchmark_object);
			benchmark_object.dpiX = 80;
			benchmark_object.dpiY = starling_round.dpiY + 50;
			
			var dpiSprite:DPISprite = new DPISprite();
			dpiSprite.dpiX = 50;
			dpiSprite.dpiY = starling_round.dpiY + starling_round.dpiH;
			addChild(dpiSprite);
			dpiSprite.dpiScaleX = 0.9;
			
			var button_normal:DPIImage = new DPIImage(DPIResource.instance.getTexture("button_normal"));
			dpiSprite.addChild(button_normal);
			button_normal.dpiH = 20;
			button_normal.scaleX = .5;
			
			var button_medium:DPIImage = new DPIImage(DPIResource.instance.getTexture("button_medium"));
			dpiSprite.addChild(button_medium);
			button_medium.dpiX = 40;
			button_medium.dpiY = button_normal.dpiY + button_normal.dpiH;
			
			var text:DPITextField = new DPITextField(200, 200, "韩贤明就是一个天才", "Verdana", 40, 0x0000ff, true);
			text.border = true;
			text.vAlign = VAlign.TOP;
			text.hAlign = HAlign.LEFT;
			text.dpiW = 210;
			text.dpiH = 100;
			text.dpiX = 10;
			text.dpiY = 10;
			addChild(text);
		}
	}

}