package scalar.resource
{
	public final class GameResource
	{
		[Embed(source="../../../assets/textures/1x/atlas.png")]
		public static var png_texture_1x:Class;
		[Embed(source="../../../assets/textures/1x/compressed_texture.atf", mimeType="application/octet-stream")]
		public static var compressed_texture_1x:Class;
		[Embed(source="../../../assets/textures/1x/atlas.xml", mimeType="application/octet-stream")]
		public static var atlas_compressed_texture_1x:Class;
		
		[Embed(source="../../../assets/textures/2x/atlas.png")]
		public static var png_texture_2x:Class;
		[Embed(source="../../../assets/textures/2x/compressed_texture.atf", mimeType="application/octet-stream")]
		public static var compressed_texture_2x:Class;
		[Embed(source="../../../assets/textures/2x/atlas.xml", mimeType="application/octet-stream")]
		public static var atlas_compressed_texture_2x:Class;
		
		public function GameResource()
		{
		}
	}
}